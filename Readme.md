xss-lock
--------

Hooks up your favorite locker to the to systemd's login manager.

Forked from https://bitbucket.org/raymonad/xss-lock.

The main difference from the original is that I stripped out all the xcb
related code, only leaving the dbus activation code. I only wanted this so I
could lock my screen when I closed my laptop, nothing else.

